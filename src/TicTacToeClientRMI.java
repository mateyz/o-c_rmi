

import java.rmi.*;
import java.rmi.registry.*;

import javafx.application.*;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.*;

public class TicTacToeClientRMI extends Application {

    private char marker;
    private boolean myTurn = false;
    private char whoseTurn = 'X';
    private Cell[][] cell = new Cell[3][3];
    private Label lblStatus = new Label("X's turn to play");
    private TicTacToeInterface ticTacToe;
    private Label lblIdentification = new Label();

    @Override
    public void start(Stage primaryStage) {
	GridPane pane = new GridPane();
	for (int i = 0; i < 3; ++i)
	    for (int j = 0; j < 3; ++j) 
		pane.add(cell[i][j] = new Cell(i,j), j, i);

	BorderPane borderPane = new BorderPane();
	borderPane.setCenter(pane);
	borderPane.setTop(lblStatus);
	borderPane.setBottom(lblIdentification);
	
	Scene scene = new Scene(borderPane, 450, 450);
	primaryStage.setTitle("TicTacToe");
	primaryStage.setScene(scene);
	primaryStage.show();
	
	new Thread( () -> {
		try {
		    initializeRMI();
		} catch (Exception ex) {
		    ex.printStackTrace();
		}
	}).start();
    }

    protected boolean initializeRMI() throws Exception {
	String host = "";

	try {
	    Registry registry = LocateRegistry.getRegistry(host);
	    ticTacToe = (TicTacToeInterface) registry.lookup("TicTacToeImpl");
	    System.out.println("Server object " + ticTacToe + " found");
	} catch (Exception ex) {
	    System.out.println(ex.getMessage());
	}

	CallBackImpl callBackControl = new CallBackImpl(this);

	if ((marker = ticTacToe.connect((CallBack)callBackControl)) != ' ') {
	    System.out.println("Connected as " + marker + " player.");
	    Platform.runLater(() ->
			      lblIdentification.setText("You are player " +
							marker));
	    return true;
	} else {
	    System.out.println("already two players connected as ");
	    return false;
	}
    }

    public void setMyTurn(boolean myTurn) {
	this.myTurn = myTurn;
    }

    public void setMessage(String message) {
	Platform.runLater(() -> lblStatus.setText(message));
    }

    public void mark(int row, int column, char token) {
	cell[row][column].setToken(token);
    }

    public class Cell extends Pane {
	private boolean marked = false;
	int row, column;
	private char token = ' ';

	public Cell(final int row, final int column) {
	    this.row = row;
	    this.column = column;
	    setStyle("-fx-border-color: black");
	    this.setPrefSize(2000,2000);
	    this.setOnMouseClicked(e -> handleMouseClick());
	}

	public char getToken() {
	    return token;
	}

	public void setToken(char c) {
	    token = c;
	    marked = true;

	    if (token == 'X') {
		Line line1 = new Line(10, 10, this.getWidth() - 10,
				      this.getHeight() - 10);
		line1.endXProperty().bind(this.widthProperty().subtract(10));
		line1.endYProperty().bind(this.heightProperty().subtract(10));

		Line line2 = new Line(10, this.getHeight() - 10,
				      this.getWidth() - 10, 10);
		line2.startYProperty().bind(this.heightProperty().subtract(10));
		line2.endXProperty().bind(this.widthProperty().subtract(10));

		Platform.runLater(() -> this.getChildren().addAll(line1, line2));
	    } else if (token == 'O') {
		Ellipse ellipse = new Ellipse(this.getWidth() / 2,
					      this.getHeight() / 2,
					      this.getWidth() / 2 - 10,
					      this .getHeight() / 2 - 10);
		ellipse.centerXProperty().bind(this.widthProperty().divide(2));
		ellipse.centerYProperty().bind(this.heightProperty().divide(2));
		ellipse.radiusXProperty().bind(this.widthProperty().divide(2).subtract(10));
		ellipse.radiusYProperty().bind(this.heightProperty().divide(2).subtract(10));
		ellipse.setStroke(Color.BLACK);
		ellipse.setFill(Color.WHITE);

		Platform.runLater(() -> getChildren().add(ellipse));
	    }
	}

	private void handleMouseClick() {
	    if (myTurn && !marked) {
		setToken(marker);

		try {
		    ticTacToe.myMove(row, column, marker);
		} catch (RemoteException ex) {
		    System.out.println(ex.getMessage());
		}
	    }
	}
    }

    public static void main(String[] args) {
	launch(args);
    }
}
